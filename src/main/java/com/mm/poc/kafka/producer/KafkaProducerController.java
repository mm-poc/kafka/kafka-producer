package com.mm.poc.kafka.producer;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.support.SendResult;
import org.springframework.util.concurrent.ListenableFuture;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.mm.poc.kafka.producer.beans.PersonBean;

@RestController()
@RequestMapping(value = "/kafka", produces = "application/json")
public class KafkaProducerController 
{
	private static final Logger logger = LoggerFactory.getLogger(KafkaProducerController.class);
	
	private static final String SUCCESS = "{'result':'SUCCESS'}";
	private static final String FAIL = "{'result':'FAIL'}";
	
    @Value(value = "${message.topic.name}")
    private String messageTopicName;
    @Autowired
    private KafkaTemplate<String, String> messageKafkaTemplate;
    @Autowired
    private KafkaTemplate<String, PersonBean> personKafkaTemplate;

    /**
	 *	NOTE: here we want to wait until the message is acknowledged before returning.
	 *	      another option is to add a callback instead which will be executed when the send transaction is finished.
	 *		    future.addCallback(new ListenableFutureCallback<SendResult<String, String>>() {	    	 
	 *		        @Override
	 *		        public void onSuccess(SendResult<String, String> result) {
	 *		            logger.info("Sent message=[" + message + "] with offset=[" + result.getRecordMetadata().offset() + "]");
	 *		        }
	 *		        @Override
	 *		        public void onFailure(Throwable ex) {
	 *		            logger.error("Unable to send message=[" + message + "] due to : " + ex.getMessage());
	 *		        }
	 *		    });		
     * @param message String
     * @return String
     */
    @PostMapping({"/message"})
	public String sendMessage(@RequestBody String message)
	{
		String result = SUCCESS;
		
		ListenableFuture<SendResult<String, String>> future = messageKafkaTemplate.send(messageTopicName, message);
		try 
		{
			SendResult<String, String> response = future.get();
			if(logger.isInfoEnabled()) {
				logger.info(response.toString());
			}
		} 
		catch(Exception e) 
		{
			logger.error("ERROR waiting for response", e);
			result = FAIL;
		}
		
		return result; 
	}
    
    @PostMapping({"/person"})
    public String sendPerson(@RequestBody PersonBean person)
    {
		String result = SUCCESS;
		ListenableFuture<SendResult<String, PersonBean>> future = personKafkaTemplate.send(messageTopicName, person);
		try 
		{
			SendResult<String, PersonBean> response = future.get();
			if(logger.isInfoEnabled()) {
				logger.info(response.toString());
			}
		} 
		catch(Exception e) 
		{
			logger.error("ERROR waiting for response", e);
			result = FAIL;
		}    	
		return result; 
    }
}
