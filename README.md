# Kafka-Producer

A simple kafka producer.

To test locally,
1. Install kafka, https://kafka.apache.org/quickstart
2. Start the zookeeper and kafka servers.
3. Start this simple kafka producer spring boot app.
4. Start command line consumer for the testmessage topic:
   bin/windows/kafka-console-consumer.bat --bootstrap-server localhost:9092 --topic testmessage
5. Post message to the producer.
   curl -X POST http://localhost:8080/kafka/message -d 'Test message1'

You should see the message you posted show up on the command line window where you are listening to the topic.